## Cassandra+Prometheus@Kubernetes

Ansible плэйбуки для разворачивания Kubernetes на две ноды (master и node) и запуска на нем Cassandra с Prometheus мониторингом

Cassandra использует https://github.com/prometheus/jmx_exporter для отдачи метрик в prometheus

Prometheus использует kubernetes api server для поиска cassandra

# На чем тестировалось

* Две одинаковые виртуалки по 2vCPU и 2GB памяти
* Ubuntu 16.04.4 x64

# Makefile targets

- `make clean` - пересоздает дроплеты в digitalocean (делал для удобства, не удалять же теперь)
Использует следующие переменные:
    - `DO_TOKEN` - API токен от digitalocean
    - `DO_MASTER_DROPLET` - id дроплета, на котором будет master
    - `DO_MASTER_IP` - внешний ip master дроплета
    - `DO_NODE_DROPLET` - id дроплета, на котором будет node
    - `DO_NODE_IP` - внешний ip node дроплета

- `make user` - создает пользователя kube. По умолчанию в образе Ubuntu в DO есть только root пользователь и поэтому для k8s лучше создать своего пользователя

    Использует следующие переменные:
    - `ROOT_SSH_USER` - пользователь, у которого есть права создавать пользователей
    - `ROOT_SSH_PRIVATE_KEY` - путь до приватного ключа от `ROOT_SSH_USER`

    Параметры в inventory:
    - `kube_user_ssh_key_public_file` - путь до public ключа, который запишется в `authorized_keys` нового пользователя

- `make k8s` - поднимает k8s на master и node

    Параметры в inventory:
    - `internal_address` у сервера в `master` группе - ip адрес сервера, по которому он доступен node виртуалке. Используется как `apiserver-advertise-address` при инициализации кластера k8s

- `make app` - поднимает cassandra и prometheus

    Параметры в inventory:
    - `prometheus_external_port` - порт, на котором будет доступен prometheus снаружи
    
# Step-by-step

1. Запустить виртуалки
2. Склонировать репозиторий
3. Поправить inventory
    - указать внешние ip адреса виртуалок
    - указать внутренний адрес master сервера
    - указать в `ansible_ssh_user` и `ansible_ssh_private_key_file` пользователя с беспарольным sudo. Если такого пользователя еще нет, то дополнительно указать путь `kube_user_ssh_key_public_file`
    - указать `prometheus_external_port`, если не нравится дефолтный
4. (Optional) Запустить `make user` для заведения нового пользователя
5. Запустить `make k8s app`
