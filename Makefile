default:
	cat README.md

clean:
ifndef DO_TOKEN
	$(error DO_TOKEN is not defined)
endif
ifndef DO_MASTER_DROPLET
	$(error DO_MASTER_DROPLET is not defined)
endif
ifndef DO_MASTER_IP
	$(error DO_MASTER_IP is not defined)
endif
ifndef DO_NODE_DROPLET
	$(error DO_NODE_DROPLET is not defined)
endif
ifndef DO_NODE_IP
	$(error DO_NODE_IP is not defined)
endif
	curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $(DO_TOKEN)" -d '{"type":"rebuild","image":"ubuntu-16-04-x64"}' "https://api.digitalocean.com/v2/droplets/$(DO_MASTER_DROPLET)/actions"
	curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $(DO_TOKEN)" -d '{"type":"rebuild","image":"ubuntu-16-04-x64"}' "https://api.digitalocean.com/v2/droplets/$(DO_NODE_DROPLET)/actions"
	ssh-keygen -f "/home/palkin/.ssh/known_hosts" -R $(DO_MASTER_IP)
	ssh-keygen -f "/home/palkin/.ssh/known_hosts" -R $(DO_NODE_IP)


user:
ifndef ROOT_SSH_USER
	$(error ROOT_SSH_USER is not defined)
endif
ifndef ROOT_SSH_PRIVATE_KEY
	$(error ROOT_SSH_PRIVATE_KEY is not defined)
endif

	ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory -e "ansible_ssh_user=$(ROOT_SSH_USER)" -e "ansible_ssh_private_key_file=$(ROOT_SSH_PRIVATE_KEY)" user.yml

k8s:
	ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory k8s.yml

app:
	ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i inventory cassandra.yml